# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

__author__ = 'Gil Olaes, Bertrand Haddad, Edbert Chan'
__email__ = 'golaes@ucsd.edu, bhaddad@ucsd.edu, eyc010@ucsd.edu'

import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        #bellman update
        for i in range(iterations):
          #we want the previous values from itterations, not concurrently
          previous_values = self.values.copy()
          for state in self.mdp.getStates():
            if self.mdp.isTerminal(state) == False:
              overall_max = -1000
              for next_state_action in mdp.getPossibleActions(state):
                #get the maximum q value out of all the possible actions
                maybe_max = self.computeQValueFromValues(state, next_state_action)
                if(maybe_max > overall_max):
                  overall_max = maybe_max
              previous_values[state] = overall_max
          self.values = previous_values


    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        #technically, our expected value is calculated across
        #all possible states hence we multiply the probabilty of each
        #across each expected value between states (eg if we have 20% A->B, we
        # attribute weight of .2 *(discount of what we already know A->B + new util)
        temp_sum = 0
        for state_prob in self.mdp.getTransitionStatesAndProbs(state,action):
          new_expected_val =  self.mdp.getReward(state,action,state_prob[0])
          discount_learn = self.discount * self.values[state_prob[0]]
          temp_sum  = temp_sum + state_prob[1]*(discount_learn + new_expected_val)
        return temp_sum

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        #itterate through all actions and get the value or wahtever
        #this is just computing argmax across all our Q values
        values_of_policy = util.Counter()
        policy = None
        for action in self.mdp.getPossibleActions(state):
          values_of_policy[action] = self.getQValue(state,action)
        policy = values_of_policy.argMax()
        return policy

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
