# analysis.py
# -----------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


######################
# ANALYSIS QUESTIONS #
######################

# Set the given parameters to obtain the specified policies through
# value iteration.

__author__ = 'Gil Olaes, Bertrand Haddad, Edbert Chan'
__email__ = 'golaes@ucsd.edu, bhaddad@ucsd.edu, eyc010@ucsd.edu'

def question2():
    #the path is straight across. Therefore, there is no need to explore
    #up or down. We just continue in the same direction we're already going
    answerDiscount = 0.9
    answerNoise = 0.0
    return answerDiscount, answerNoise

def question3a():
    # to risk the cliff, we want to make no distinction between empty squares
    #and the cliff. We offer no reward for living or at the very least
    #we want to force it to find the exit sooner so we increase the penalty
    #for living

    #we also want to strongly remember the path we came so that we can
    #retake it

    #finally, we want to reasonably explore but not explore too much.
    #if we explore too much, we end up deviating further and further away from
    #the closest posslbe
    answerDiscount = .6
    answerNoise = .3
    answerLivingReward = -5
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3b():

    #we decrease the discount because we want to increase

    #we decrease the noise because we dont want to risk the
    #cliff or explore away from our current path

    #we increase the living reward b/c we want to avoid 
    #taking the cliff over exploring elsewhere but we still
    #dont want it to explore forever so we keep it negative
    answerDiscount = .3
    answerNoise = .1
    answerLivingReward = -3
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3c():
    #decrease noise to avoid cliff
    #increase discount b/c we like what we learn
    #increase living reward b/c we like to keep exploring

    answerDiscount = .9
    answerNoise = 0.05
    answerLivingReward = 0.0
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3d():
    #same as above between a and b
    answerDiscount = .6
    answerNoise = .2
    answerLivingReward = -.5
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3e():
    #safe safe!
    answerDiscount = 1
    answerNoise = 0.0
    answerLivingReward = 100
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question6():
    answerEpsilon = None
    answerLearningRate = None
    return 'NOT POSSIBLE' #answerEpsilon, answerLearningRate
    # If not possible, return 'NOT POSSIBLE'

if __name__ == '__main__':
    print 'Answers to analysis questions:'
    import analysis
    for q in [q for q in dir(analysis) if q.startswith('question')]:
        response = getattr(analysis, q)()
        print '  Question %s:\t%s' % (q, str(response))
